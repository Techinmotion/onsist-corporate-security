# Cyber Security Made Even Easier #

Be sure that all devices dealing with the company network or any company data have reliable anti-virus and anti-malware software. This is a basic, but easily overlooked, precaution against malicious files and other attacks. Your network should also have a firewall to protect the network as a whole.

· Educate your employees. In addition to making sure that everyone in your company is familiar with your security system, it may be useful to train employees on basic Internet safety and security. There are lots of online resources that raise awareness about phishing scams, security certificates, and other cyber security basics.

· Create strong passwords. For any resources requiring passwords on your system, create (and have employees create) complex passwords that aren't subject to social engineering or easy guessing. There are a number of guides available on the web about how to create strong passwords.

· Use encryption software if you deal with sensitive information on a regular basis. That way, even if your data is compromised, the hacker won't be able to read it.

· Limit administrator privileges to your system. Set up the proper access boundaries for employees without administrator status, especially when using non-company devices. Limit administrator privileges to those who really need them, and limit access to sensitive information by time and location.

· Look into cyberinsurance. Cyber security breaches generally aren't covered by liability insurance, but if you're looking to protect sensitive data, talk to an insurance agent about your options.

· Back up your data weekly, either to a secure cloud location or to an external hard drive. That way, if your server goes down, you'll still have access to your data. Boardroom Executive Suites' Cloud Computing Services by SkySuite are an ideal tool in this area.

· If you've determined that there was a security breach, figure out the scope of the attack. This is a good time to call in a consultant who is an expert in cyber security. This will both give you a sense of what damage you need to mitigate and point to whether it was a generic mass-produced attack or a specifically targeted one.

· Once you've conducted this investigation, pull all of your systems offline to contain the damage.

· Repair affected systems. You can use master discs to reinstall programs on your devices. Then, with the help of your consultant, figure out where the gaps are in your security system. To prevent another attack from happening, use this as a learning experience to make your protection stronger. This likely includes educating your employees on what went wrong and what they can do in the future to stop that from happening.

· Be honest, transparent, and timely in your communication with your customers. Let them know what happened and what you're doing to fix it

* [https://www.onsist.com/corporate-security/](https://www.onsist.com/corporate-security/)

